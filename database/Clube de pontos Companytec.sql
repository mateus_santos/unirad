-- -----------------------------------------------------
-- Schema clube_pontos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clube_pontos` DEFAULT CHARACTER SET utf8 ;
USE `clube_pontos` ;

-- -----------------------------------------------------
-- Table `clube_pontos`.`pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`pedido` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(15) NOT NULL,
  `usuario_id` INT(11) NOT NULL,
  `total` INT(11) NOT NULL,
  `data` DATETIME NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '1',
  `colaborador_id` INT(11) NULL DEFAULT NULL,
  `obs` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`produto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NOT NULL,
  `descricao` VARCHAR(150) NOT NULL,
  `imagem` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`carrinho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`carrinho` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` INT(11) NOT NULL,
  `produto_id` INT(11) NOT NULL,
  `quantidade` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Pedidos_has_produto_produto1_idx` (`produto_id` ASC),
  INDEX `fk_Pedidos_has_produto_Pedidos_idx` (`pedido_id` ASC),
  CONSTRAINT `fk_Pedidos_has_produto_Pedidos`
    FOREIGN KEY (`pedido_id`)
    REFERENCES `clube_pontos`.`pedido` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_has_produto_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `clube_pontos`.`produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`campanha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`campanha` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `desc_maximo` DECIMAL(10,2) NOT NULL,
  `dt_inicial` DATE NOT NULL,
  `dt_final` DATE NOT NULL,
  `dias_bloqueio` INT(11) NOT NULL,
  `razao_pontos` DECIMAL(5,2) NOT NULL,
  `dias_expiracao` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`nota_fiscal_dev_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`nota_fiscal_dev_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`nota_fiscal_dev_atribuida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`nota_fiscal_dev_atribuida` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nota_id` INT(11) NOT NULL,
  `colaborador_id` INT(11) NOT NULL,
  `data` DATETIME NOT NULL,
  `responsavel_id` INT(11) NULL DEFAULT NULL,
  `dt_confirmacao` DATETIME NULL DEFAULT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_nota_fiscal_dev_atribuida_status_nota_fiscal_dev1_idx` (`status_id` ASC),
  CONSTRAINT `fk_nota_fiscal_dev_atribuida_status_nota_fiscal_dev1`
    FOREIGN KEY (`status_id`)
    REFERENCES `clube_pontos`.`nota_fiscal_dev_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`campanha_produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`campanha_produto` (
  `produto_id` INT(11) NOT NULL,
  `campanha_id` INT(11) NOT NULL,
  `pontos` INT NOT NULL,
  `qtd_disponivel` INT NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT 1,
  `razao_ponto` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`produto_id`, `campanha_id`),
  INDEX `fk_produto_has_campanha_campanha1_idx` (`campanha_id` ASC),
  INDEX `fk_produto_has_campanha_produto1_idx` (`produto_id` ASC),
  CONSTRAINT `fk_produto_has_campanha_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `clube_pontos`.`produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_produto_has_campanha_campanha1`
    FOREIGN KEY (`campanha_id`)
    REFERENCES `clube_pontos`.`campanha` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clube_pontos`.`nota_fiscal_cancel_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`nota_fiscal_cancel_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`nota_fiscal_saida_cancelada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`nota_fiscal_saida_cancelada` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nota_id` INT NOT NULL,
  `colaborador_id` INT NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  `responsavel_id` INT NOT NULL,
  `dt_confirmacao` DATETIME NOT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_nota_fiscal_saida_cancelada_status_nota_fiscal_cancel1_idx` (`status_id` ASC),
  CONSTRAINT `fk_nota_fiscal_saida_cancelada_status_nota_fiscal_cancel1`
    FOREIGN KEY (`status_id`)
    REFERENCES `clube_pontos`.`nota_fiscal_cancel_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`transferencia_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`transferencia_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`transferencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`transferencia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `usuario_id_destinatario` INT NOT NULL,
  `usuario_id_remetente` INT NOT NULL,
  `pontos` INT NOT NULL,
  `data` DATETIME NOT NULL,
  `dt_confirmacao` DATETIME NOT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transferencia_transferencia_status1_idx` (`status_id` ASC),
  CONSTRAINT `fk_transferencia_transferencia_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `clube_pontos`.`transferencia_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`log_cvc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`log_cvc` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `usuario_id` INT NOT NULL,
  `colaborador_id` INT NOT NULL,
  `data` DATETIME NOT NULL,
  `descricao` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
