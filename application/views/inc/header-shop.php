﻿<!doctype html>
<html class="no-js" lang="pt-br">

<head>    
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1"/>
    <meta name="author" content="Theme Industry">
    <!-- description -->
    <meta name="description" content="reone">
    <!-- keywords -->
    <meta name="keywords" content="">
    <!-- title -->
    <title>Companytec - <?php echo $title;?></title>
    <!-- favicon -->
    <link rel="icon" href="<?php echo base_url('bootstrap/images/favicon.ico'); ?>">
    <!-- animation -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/animate.min.css'); ?>"/>
	
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/jquery-ui.min.css'); ?>"/>
    
	<!-- bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>"/>
    <!-- font-awesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/font-awesome.min.css'); ?>"/>
    <!-- magnific popup -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/magnific-popup.min.css'); ?>"/>
    <!-- cube Portfolio -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/jquery.fancybox.min.css'); ?>"/>
	
	
    <!-- revolution slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/revolution/css/settings.css'); ?>"/>
    <!-- owl carousel -->
    <link href="<?php echo base_url('bootstrap/css/owl.carousel.min.css'); ?>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url('bootstrap/css/owl.theme.default.min.css'); ?>" rel="stylesheet" type="text/css" media="all"/>
    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/swiper.min.css'); ?>">
    <!-- component.css push nav-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/component.css'); ?>"/>
    <!-- bundle css -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/core.css'); ?>"/>
    <!-- style -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/style.css'); ?>"/>
    <!-- style -->

    <!-- Custom Style -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/custom.css'); ?>"/>


</head>
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<body>

<div id="loader">
	<div class='loader'></div>
</div>
<div id="carregar" style="display: none;">
	<p style="position: absolute;left: 48%;top: 53%;font-size:16px;">Carregando...</p>
</div>
<div style="display: none;" id="loader_checkout">
	<img src="https://www.companytec.com.br/fechar_pedido.gif" />
</div>
<?php
	
    $total_qtd_carrinho = 0;    
    $mostra = 'display: none;';
	if(isset($pedido['carrinho'])){
		if(isset($pedido['carrinho']) > 0){
			foreach($pedido['carrinho'] as $total){
				$total_qtd_carrinho = $total['quantidade'] + $total_qtd_carrinho;       
			}		
		}	
		$mostra = (count( $pedido['carrinho']) > 0) ?  "display: block;" : "display: none;";
	}
	
	
	
?>                                 
<!-- Push Nav -->
<div id="perspective" class="other perspective effect-moveleft">
    <div class="cont">
        <div class="wrapper">
            <!-- start header -->
            <header>
                <!-- start navigation -->
                <nav class="navbar navbar-default bootsnav navbar-fixed-top nav-white header-light bg-transparent nav-box-width on no-full nav_line">
                    <div class="nav-header-container">
                        <div class="row">
                            <a href="https://www.companytec.com.br" title="Logo" class="logo">
								<img src="<?php echo base_url('bootstrap/images/companytec2.png'); ?>" class="logo-dark" alt="reone" />
								<img src="<?php echo base_url('bootstrap/images/companytec1.png'); ?>" alt="reone" class="logo-light default">
							</a>
                            <div class="carrinho-pontos">
								
                                <div class="carrinho-qtd-prd" style="<?php echo $mostra;?>">
                                    <p class="carrinho-qtd" total="<?php echo $total_qtd_carrinho;?>">
                                        <?php echo $total_qtd_carrinho;?>                                        
                                    </p>
                                </div>
                                <p class="text-small text-blue"     style="width: 102%;">
									<span id="ola">
									Olá <span style="font-weight: 900;"><?php echo $nome;?></span> Você possui <span style="font-weight: 900;"><?php echo $pontos;?></span> Pontos.</span>
									
									<a class="icone-shopping" data-toggle="tooltip" data-placement="auto left" title="Histórico pontos">
										<i class="fa fa-clock-o historico text-large"></i>
									</a>
									<a href="<?php echo base_url('clubedevantagens/transferencia'); ?>" class="icone-shopping" data-toggle="tooltip" data-placement="auto left" title="Transferir pontos">
										<i class="fa fa-exchange text-large"  ></i>
									</a>
                                    <a href="<?php echo base_url('clubedevantagens/checkout'); ?>" class="icone-shopping carrinho" title="Carrinho de Compras" data-toggle="tooltip" data-placement="auto left">
                                        <i class="fa fa-shopping-cart text-large" ></i>
                                    </a>
									<a class="icone-shopping" data-toggle="tooltip" data-placement="auto left" title="Sair" href="<?php echo base_url('clubedevantagens/logout'); ?>">
										<i class="fa fa-sign-out text-large" ></i>
									</a>									
                                </p>
                            </div>
							
                        </div>
                    </div>
                </nav>
                <!-- Finalizar carrinho -->
                <div id="carrinho-finalizar" class="showMenuButton-two" style="<?php echo $mostra;?>"> 
                    <p class="text-small" style="border-bottom: 1px solid #1761ac;text-align: center;font-weight: 600;padding-bottom: 6px;">Ítens do carrinho</p>
                        <table >
                            <?php if(isset($pedido['carrinho'])){
									if(count($pedido['carrinho']) > 0){ ?>
                            <?php 		foreach($pedido['carrinho'] as $carrinho){     ?>
                                <tr style="border-bottom: 1px solid #ffcc00;" produto="<?php echo $carrinho['produto_id']['id']; ?>">
                                    <td width="15%" style="padding-bottom: 15px;padding-top: 15px;">
                                        <input type="hidden" class="produto_id_ativo" value="<?php echo $carrinho['produto_id']['id']; ?>">
                                        <img src="<?php echo base_url('imagens-produtos/'.$carrinho['produto_id']['imagem']); ?>" class="img-fluid"   />
                                    </td>                            
                                    <td class="text-extra-small" width="55%" style="text-align: center;">
                                        <?php echo $carrinho['produto_id']['descricao']; ?>
                                    </td>                                
                                    <td class="text-extra-small" width="10%" style="text-align: center;">
                                        <i class="fa fa-minus text-extra-small text-blue"  onclick="removerQtdItem('<?php echo $carrinho['produto_id']['id']; ?>');"></i>
                                        <span class="qtd-<?php echo $carrinho['produto_id']['id']; ?>"> <?php echo $carrinho['quantidade']; ?> </span>
                                        <input type="hidden" produto_id="<?php echo $carrinho['produto_id']['id']; ?>"  class="produto_qtd_ativo"   value="<?php echo $carrinho['quantidade']; ?>">
                                        <i class="fa fa-plus text-extra-small text-blue" onclick="adicionaQtdItem('<?php echo $carrinho['produto_id']['id']; ?>');"></i>
                                    </td>
                                    <!--<td class="text-extra-small pontos" width="20%" style="text-align: right;">
                                        <?php echo $carrinho['produto_id']['pontos']; ?> pts
                                    </td>-->
                                </tr>
                            <?php } ?>
                        <?php }
							}
						?>
                        </table>
                        <div class="carrinho-finalizar">
                            <a class="btn btn-blue btn-large text-lager margin-10px-bottom" href="<?php echo base_url('clubedevantagens/checkout'); ?>" >Finalizar Resgate</a>
                        </div>
                 </div>

                
            </header>
            <!-- end header -->

<!-- shop cover-->
<section class="bg blog-cover" style="padding: 50px 0px !important">
    <div class="container" style="max-height: 369px;">
        <div class="text-center" >
            <a href="https://www.companytec.com.br/clubedevantagens/"><img src="<?php echo base_url('bootstrap/images/FundoBannerLogo.png'); ?>" class="img-fluid" style="max-width: 350px;max-height: 288px;" /> </a>
            <!--<div class="page_nav">
                <span class="text-white">You are here:</span> <a href="index.html" class="text-white">Home</a> <span class="text-white"><i class="fa fa-angle-double-right"></i> Blog</span>
            </div>-->
        </div>
    </div>
</section>
