 <!-- footer -->
            <footer class="padding-40px-tb bg-blue"  >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="text-small text-white xs-text-center xs-margin-15px-bottom">© <?php echo date('Y'); ?> Clube de Vantagens 
                                <a href="#." class="text-white">Companytec</a>
                            </div>
                        </div>
                        <div class="col-sm-5 no-padding">
                                <div class="text-small text-white xs-text-center xs-margin-15px-bottom">
									<a href="https://www.companytec.com.br/Downloads/DT774-ManualdeOrientacaoCampanhaClubedeVantagensCompanytec.pdf" class="text-white">Regras e Termos Do Clube de Vantagens Companytec
									</a>
								</div>
                        </div>
                        <div class="col-sm-3">
                            <div class="footer-social text-right xs-text-center xs-margin-10px-top">
                                <a href="https://www.facebook.com/companytec/" class="facebook-text-hvr"><i class="fa fa-facebook" aria-hidden="true"></i></a>                          
								<a href="https://www.instagram.com/companytec/" class="instagram-text-hvr"><i class="fa fa-instagram" aria-hidden="true"></i></a>                              
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- footer end--> 

            <!-- start scroll to top -->
            <a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
            <!-- end scroll to top  -->

</div>
</div>

</div>






<!-- javascript libraries -->
<script src="<?php echo base_url('bootstrap/js/jquery-3.2.1.js');?>"></script>
<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('bootstrap/js/jquery.appear.min.js');?>"></script>
<!-- parallax -->
<script src="<?php echo base_url('bootstrap/js/parallaxie.min.js');?>"></script>
<!-- owl carousel -->
<script src="<?php echo base_url('bootstrap/js/owl.carousel.min.js');?>"></script>
<!-- magnific popup -->
<script src="<?php echo base_url('bootstrap/js/jquery.magnific-popup.min.js');?>"></script>
<!-- flip -->
<script src="<?php echo base_url('bootstrap/js/jquery.flip.min.js');?>"></script>
<!-- fancybox -->
<script src="<?php echo base_url('bootstrap/js/jquery.fancybox.min.js');?>"></script>
<!-- Swiper Core Javascript -->
<script src="<?php echo base_url('bootstrap/js/swiper.min.js');?>"></script>
<!-- Morphext js -->
<script src="<?php echo base_url('bootstrap/js/morphext.min.js');?>"></script>
<!-- wow -->
<script src="<?php echo base_url('bootstrap/js/wow.js'); ?>"></script>
<!-- isotope.pkgd.min js -->
<script src="<?php echo base_url('bootstrap/js/isotope.pkgd.min.js');?>"></script>
<!-- push nav -->
<script src="<?php echo base_url('bootstrap/js/push_nav.js');?>"></script>
<!-- revolution -->
<script src="<?php echo base_url('bootstrap/revolution/js/jquery.themepunch.tools.min.js');?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/jquery.themepunch.revolution.min.js');?>"></script>
<!-- revolution extension -->
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.actions.min.js');?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.carousel.min.js');?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.kenburn.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.layeranimation.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.migration.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.navigation.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.parallax.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.slideanims.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/revolution/js/extensions/revolution.extension.video.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/js/plugins/notify.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/js/plugins/jquery-ui.min.js'); ?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url('bootstrap/js/jquery.mask.min.js'); ?>"></script>


<!-- setting -->
<script src="<?php echo base_url('bootstrap/js/main.js');?>"></script>
<?php
// carregar js particular de cada View
$js = explode('/', $view); 
$loadjs = $js[count($js) - 1].'.js';    
?>
<script src="<?=base_url('bootstrap/js/'.$loadjs)?>" type="text/javascript"></script>
</body>
</html>