<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoUsuarios/');?>">
		<i class="m-menu__link-icon la la-users"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão Usuários</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoVideos/');?>">
		<i class="m-menu__link-icon la la-file-movie-o"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Vídeos</span>
			</span>
		</span>
	</a>
</li>