<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaCliente/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaCliente/todosVideos/');?>">
		<i class="m-menu__link-icon la la-file-movie-o"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Todos os Vídeos</span>
			</span>
		</span>
	</a>
</li>