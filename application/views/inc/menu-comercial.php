<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('PedidosRealizados/index')?>" class="m-menu__link ">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Pedidos Realizados</span>
			</span>
		</span>
	</a>
</li>							
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('Parceiro')?>" class="m-menu__link ">
		<i class="m-menu__link-icon fa fa-users"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Parceiros</span>
			</span>
		</span>
	</a>
</li>	
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('Notas/canceladas')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-cancel"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Notas Canceladas</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('Notas/devolvidas')?>" class="m-menu__link ">
		<i class="m-menu__link-icon la la-rotate-left"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Notas Devolvidas</span>
			</span>
		</span>
	</a>
</li>							