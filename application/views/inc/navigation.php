<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
			<!-- BEGIN: Aside Menu -->
			<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500" >
			
			<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
				
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Campanha/gestaoCampanhas')?>" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-gears"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gestão de Campanhas</span>
							</span>
						</span>
					</a>
				</li>						
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Produto/gestaoProdutos')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-tags"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gestão de Produtos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Transferencia/index')?>" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-exchange"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Transferências de Pontos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('PedidosRealizados/index')?>" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-shopping-cart"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Pedidos Realizados</span>
							</span>
						</span>
					</a>
				</li>							
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Parceiro')?>" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-users"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Parceiros</span>
							</span>
						</span>
					</a>
				</li>	
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Notas/canceladas')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-cancel"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Notas Canceladas</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('Notas/devolvidas')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-rotate-left"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Notas Devolvidas</span>
							</span>
						</span>
					</a>
				</li>							
			</ul>			
		</div>
		<!-- END: Aside Menu -->
	</div>
	<!-- END: Left Aside -->							
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->