<?php 

Class UsuariosModel extends CI_Model {
	

    public function inserir($usuarios){
        $this->db->insert('usuarios', $usuarios);
        return $this->db->insert_id();
    }
    

    public function atualizar($usuarios){
        $this->db->where('id', $usuarios['id']);
        return $this->db->update('usuarios', $usuarios);
    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        
		if($this->db->delete('usuarios')){			
			return true;
			
        }else{
            return false;
        }
		
		
    }
   
   public function getEmail($email)
   {
       $sql = "SELECT count(*) as total FROM usuarios WHERE email ='".$email."'";
       return $this->db->query($sql)->row_array();
   }

    public function autenticacao($email,$senha)
    {
        $sql = "SELECT  count(*) as total, u.nome, u.email,u.tipo_cadastro_id, tc.descricao as tipo_acesso, u.id, u.foto, u.whatsapp as telefone_usuario
                FROM    usuarios u                
                INNER JOIN  tipo_cadastros tc  ON  tc.id = u.tipo_cadastro_id                
                WHERE u.email = '".$email."' and u.senha = '".md5(md5($senha))."' and u.ativo = 1
                GROUP BY u.nome, u.email, u.tipo_cadastro_id, u.id";              
        
        $query = $this->db->query($sql);

        return $query->result();
    }

    /* Método para verificar se existe usuário  *
     * @params: $nome, primeiro nome do usuário *
     * @params: $email, email do usuário        *
     * @view:   esqueci-minha-senha.php         *
     * @controller: esqueci-minha-senha.php     */
    public function verificaUsuario($cpf,$email)
    {
        $sql = "SELECT count(*) as total, u.id, u.nome FROM usuarios u WHERE email = '".$email."' and cpf = '".$cpf."' group by u.id,u.nome";              
        $query = $this->db->query($sql);

        return $query->result();   
    }

    public function redefinirSenha($id,$senha)
    {
        $dados = array('senha' => md5(md5($senha)));
        
        $this->db->where('id', $id);
        $retorno = $this->db->update('usuarios', $dados);        
        return $retorno;
    }
   
    public function getUsuarios()
    {
        
        $sql    =   "SELECT     u.id,u.nome,u.cpf,u.email,tp.descricao,u.dthr_cadastro,u.ativo 
                        FROM    usuarios u, tipo_cadastros tp  
                        WHERE   u.tipo_cadastro_id = tp.id";
        $query  =   $this->db->query($sql);
        return $query->result();

    }

    public function atualizaStatus($dados)
    {
        $update = array(
            'ativo' => $dados['ativo']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function getUsuario($id){

        $sql =  "SELECT u.* 
                    FROM usuarios u                     
                    WHERE u.id=".$id;
        $query = $this->db->query($sql)->row_array();
        return $query;
    }

    public function getTiposCadastro()
    {

        $sql        =   "SELECT * FROM tipo_cadastros ORDER BY descricao ASC";
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();
        return $retorno;
    }

    public function atualizaUsuario($dados)
    {
        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $dados)){
            return true;
        }else{
            return false;
        }
    }

     public function atualizaCliente($dados)
    {
        $update = $dados;

        if(isset($dados['foto'])) {
            $update['foto'] = $dados['foto'];
        }

        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $update)){
            return true;
        }else{
            return false;
        }
    }


}
