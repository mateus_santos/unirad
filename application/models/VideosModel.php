<?php 

Class VideosModel extends CI_Model {
	
	public function inserir($videos){
        $this->db->insert('videos', $videos);
        return $this->db->insert_id();
    }
    

    public function atualizar($videos){
        $this->db->where('id', $videos['id']);
        return $this->db->update('videos', $videos);
    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        
		if($this->db->delete('videos')){			
			return true;			
        }else{
            return false;
        }		
		
    }

	public function buscaVideos()
	{
		$sql = "SELECT v.*, u.nome FROM videos v
				INNER JOIN usuarios u ON v.usuario_id = u.id";

		return $this->db->query($sql)->result_array();
	}

	public function getVideo($id)
	{
		$sql = "SELECT v.*, u.nome FROM videos v
				INNER JOIN usuarios u ON v.usuario_id = u.id
				where v.id =".$id;		

		return $this->db->query($sql)->row_array();			
	}

	public function buscaUltimoVideoUsuario($usuario_id)
	{
		$sql = "SELECT v.* FROM videos v WHERE v.usuario_id=".$usuario_id." ORDER BY v.id DESC LIMIT 1";
		return $this->db->query($sql)->row_array();
	}

	public function buscaVideosUsuario($usuario_id)
	{
		$sql = "SELECT v.* FROM videos v WHERE v.usuario_id=".$usuario_id." ORDER BY v.id DESC";
		return $this->db->query($sql)->result_array();
	}

}