<?php 

Class Campanha_Model extends CI_Model {
	

    public function inserir($campanha){
        $this->db->insert('cvc_campanha', $campanha);
        return $this->db->insert_id();
    }

    public function find($id){
        $this->db->where('id', $id);		
        return $this->db->get('cvc_campanha')->row_array();
    }

    public function buscarTodos(){
		$this->db->order_by('id', 'desc');
		$this->db->order_by('dt_inicial', 'desc');		
        return $this->db->get('cvc_campanha')->result_array();
    }
    

    public function atualizar($campanha){
        $this->db->where('id', $campanha['id']);
        return $this->db->update('cvc_campanha', $campanha);
    }

    public function excluir($id){
        
        $this->db->where('campanha_id', $id);
        
		if($this->db->delete('cvc_campanha_produto')){
			
			$this->db->where('id', $id);        
			if($this->db->delete('cvc_campanha')){
				return true;
			}else{
				return false;
			}
        }else{
            return false;
        }
		
		
    }
    
    public function findDefault(){
        return $this->db->where('id', 1)->get('cvc_campanha')->row_array();
    }
	
	public function findDate($date){
		
				$this->db->where('id !=', 1);
				$this->db->where('dt_inicial <=', $date);
				$this->db->where('dt_final >=', $date);				
				$this->db->order_by('id', 'desc');
		return	$this->db->get('cvc_campanha')->row_array();

    }
}
