<?php 

Class PagamentosModel extends CI_Model {
	

    public function inserir($pagamentos){
        $this->db->insert('pagamentos', $pagamentos);
        return $this->db->insert_id();
    }
    

    public function atualizar($pagamentos){
        $this->db->where('id', $pagamentos['id']);
        return $this->db->update('pagamentos', $pagamentos);
    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        
		if($this->db->delete('pagamentos')){			
			return true;
			
        }else{
            return false;
        }
		
		
    }

    public function buscaCompras($id)
    {
        $sql = "SELECT p.*, s.descricao as status FROM pagamentos p 
                INNER JOIN usuarios u on u.id = p.usuario_id
                inner join status s on  s.id = p.status_id
                where p.usuario_id = ".$id;
                
        return $this->db->query($sql)->result_array();
    }
   


}
