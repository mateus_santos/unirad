<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaAdministrador extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'Administrador' )) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');	
		$this->load->model('PagamentosModel', 'pagamentosM');
		$this->load->model('VideosModel', 'videosM');
		$this->load->helper('form');
		$this->load->helper('url');

	}
 
	public function index()
	{
		$parametros				= 	$this->session->userdata();
		$parametros['title']	=	"Área do Administrador";
		$this->_load_view('area-administrador/index',$parametros);
	}	

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array( 	'id'				=>	$this->input->post('id'),
								'nome'				=>	$this->input->post('nome'),
								'cpf'				=>	$this->input->post('cpf'),
								'email'				=>	$this->input->post('email'),
								'whatsapp'			=>	$this->input->post('whatsapp'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();				
				$this->_load_view('area-administrador/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$this->_load_view('area-administrador/editar-cliente',	$parametros );	
		}
	
	}

	public function gestaoUsuarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Gestão de Usuários";
		$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Alterar status do usuario ********************
	*****************************************************************************/
	public function alteraStatus()
	{
		
		$dados = array(	'id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']	);

		if($this->usuariosM->atualizaStatus($dados)){			
			if($_POST['ativo'] == 1){

				if($this->enviaEmailConfirmacao($_POST['email'])){

					echo json_encode(array('retorno' => 'sucesso'));
				}else{

					echo json_encode(array('retorno' => 'e-mail não enviado'));
				}
			}else{

				echo json_encode(array('retorno' => 'sucesso'));
			}
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}

	}
	

	private function enviaEmailConfirmacao($email_destino){

 		$email = '	<html>
						<head></head>
						<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
							<div class="content" style="width: 600px; height: 100%;">
								<img src="http://www.wertco.com.br/bootstrap/img/confirmacaoExpopostos.png" />
							</div>
						</body>
					</html>';	
		
		$this->load->library('email');
		
		$result = $this->email
		    ->from('fale-conosco@grupoinspire.com')
		    ->reply_to('fale-conosco@grupoinspire.com')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Inspire - Confirmação de Acesso')
		    ->message($email)
		    ->send();

		return $result;	

 	}

	/****************************************************************************
	*****************************************************************************
	**************** Método Responsável por Editar Usuários	*********************
	*****************************************************************************
	*****************************************************************************/
	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);			

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){								
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();
				$parametros['planos'] 	= 	$this->pagamentosM->buscaCompras($this->input->post('id'));
				$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',	$parametros );
			}
		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($id);
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['planos'] 				= 	$this->pagamentosM->buscaCompras($id);

			$this->_load_view('area-administrador/gestao-usuario/editar-usuario',	$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Produto ******************************
	*****************************************************************************/
	public function excluirProduto()
	{

		if($this->produtosM->excluirProduto($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}

	public function gestaoVideos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->videosM->buscaVideos();
		$parametros['title']	=	"Gestão de Vídeos";
		$this->_load_view('area-administrador/gestao-videos/videos',$parametros);
	}

	public function cadastraVideos()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = $this->input->post();
			unset($dados['salvar']);
			if( $this->videosM->inserir($dados) ){
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}

			$this->gestaoVideos();

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Vídeos";
			$parametros['usuarios']	=	$this->usuariosM->getUsuarios();			
			$this->_load_view('area-administrador/gestao-videos/cadastra-video',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os Fretes da WERTCO *************
	*****************************************************************************/

	public function editarVideo($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'			=>	$this->input->post('id'),
							'descricao'		=>	$this->input->post('descricao'),
							'titulo'		=> 	$this->input->post('titulo'),
							'youtube' 		=> 	$this->input->post('youtube'),
							'usuario_id'	=> 	$this->input->post('usuario_id') 	);
						 
			if($this->videosM->atualizar($update)){
				
				$this->session->set_flashdata('sucesso', 'ok');				
				$this->gestaoVideos();

			}else{

				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoVideos();
			}

		}else{

			
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->videosM->getVideo($id);
			$parametros['usuarios']	=	$this->usuariosM->getUsuarios();	
			$parametros['title']	=	"Editar Vídeo";			
			$this->_load_view('area-administrador/gestao-videos/editar-videos',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Vídeos *******************************
	*****************************************************************************/
	public function excluirVideo()
	{																								
		if($this->videosM->excluir($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

        $this->load->library('email');        

        if($anexo == null){
            $result = $this->email
                ->from('fale-conosco@grupoinspire.com')                
                ->to($email_destino)
                ->subject($titulo)
                ->message($conteudo)            
                ->send();
        }else{
            $result = $this->email
                ->from('fale-conosco@grupoinspire.com')                
                ->to($email_destino)
                ->subject($titulo)
                ->message($body)
                ->attach($anexo)            
                ->send();
        } 

        return $result;     
    }
	
}
