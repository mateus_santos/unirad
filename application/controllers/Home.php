<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();       		
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('PagamentosModel', 'pagamentosM');
    }
    
    public function index(){ 		
	
		$this->load->view('home/index');
	
	}		

	public function faleConosco(){
		$conteudo = "<body style='text-align: center;margin: 0 auto; background: #1761ac; color: #ffffff; font-size: 18px; width: 545px;'>
						<table>
							<tr>
								<td><img src='".base_url('bootstrap/images/logoTopo.png')."'></td>
							</tr>
							<tr>
								<td><h4>Formulário fale conosco respondido</h4></td>
							</tr>
							<tr>
								<td>Nome: ".$this->input->post('nome')."</td>
							</tr>
							<tr>
								<td>E-mail: <span style='color: #fff !important;'>".$this->input->post('email')."</span></td>
							</tr>
							<tr>
								<td>Telefone: ".$this->input->post('telefone')."</td>
							</tr>							
							<tr>
								<td>Mensagem: ".$this->input->post('mensagem')."</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					</body>";
		if( $this->enviaEmail('contato@grupoinspire.com','Inspire! Fale conosco.', $conteudo) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function pagamento()
	{
		$url = "https://ws.pagseguro.uol.com.br/v2/sessions?email=felipe.aguirre.goncalves@gmail.com&token=b7f1d2c4-c813-4f1b-806d-ed5711d21909b3799c454a47bd69ced37029724fba5d3eef-5eaa-49f3-90aa-28677b46e36b";
		//$url = "https://ws.sandbox.pagseguro.uol.com.br/v2/sessions?email=mateus.santos@gmail.com&token=C79B203A0ECD460CA15240FFD5C2D374";
		//http://br2.php.net/manual/pt_BR/function.curl-setopt.php
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded; charset=UTF-8"));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$retorno = curl_exec($curl);
		curl_close($curl);

		$xml = simplexml_load_string($retorno);
		echo json_encode($xml);
	}
	
    private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

        $this->load->library('email');        

        if($anexo == null){
            $result = $this->email
                ->from('fale-conosco@grupoinspire.com')                
                ->to($email_destino)
                ->subject($titulo)
                ->message($conteudo)            
                ->send();
        }else{
            $result = $this->email
                ->from('fale-conosco@grupoinspire.com')                
                ->to($email_destino)
                ->subject($titulo)
                ->message($body)
                ->attach($anexo)            
                ->send();
        } 

        return $result;     
    }

    public function insereUsuario()
    {
    	$dadosInsert = $this->input->post();
    	$dadosInsert['senha'] = md5(md5($dadosInsert['senha']));
    	$dadosInsert['cpf'] = '';
    	$usuario_id = $this->usuariosM->inserir($dadosInsert);

    	if($usuario_id > 0){

	    	echo json_encode(array( 	'retorno' 		=> 	'sucesso',
	    								'usuario_id' 	=> 	$usuario_id) );
	    }else{
	    	echo json_encode(array( 	'retorno' 		=> 	'erro') );
	    }
    }

    public function processarPagto()
    {
    	$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);

		$dadosArray["email"] 	= 	'felipe.aguirre.goncalves@gmail.com';
		$dadosArray["token"] 	= 	'b7f1d2c4-c813-4f1b-806d-ed5711d21909b3799c454a47bd69ced37029724fba5d3eef-5eaa-49f3-90aa-28677b46e36b';

		$name = preg_replace('/\d/', '', $dados['senderName']);
		$name = preg_replace('/[\n\t\r]/', ' ', $name);
		$name = preg_replace('/\s(?=\s)/', '', $name);
		$name = trim($name);
		$name = explode(' ', $name);
		 
		if(count($name) == 1 ) {
		    $name[] = ' dos Santos';
		}
		$name = implode(' ', $name);
		//echo  $dados['qntParcelas'];die;
		$dadosArray['paymentMode'] = 'default';
		$dadosArray['paymentMethod'] = $dados['paymentMethod'];
		$dadosArray['receiverEmail'] = 'felipe.aguirre.goncalves@gmail.com';
		//$dadosArray['receiverEmail'] = 'mateus.santos@gmail.com';
		$dadosArray['currency'] = $dados['currency'];
		$dadosArray['extraAmount'] = '';
		$dadosArray['itemId1'] = $dados['itemId1'];
		$dadosArray['itemDescription1'] = $dados['itemDescription1'];
		$dadosArray['itemAmount1'] = $dados['itemAmount1'];
		$dadosArray['itemQuantity1'] = $dados['itemQuantity1'];
		$dadosArray['notificationURL'] = base_url('Home/notificacao');
		$dadosArray['reference'] = $dados['reference'];
		$dadosArray['senderName'] = $name;
		//$dadosArray['senderCPF'] = $dados['senderCPF'];
		$dadosArray['senderCPF'] = $dados['creditCardHolderCPF'];
		$dadosArray['senderAreaCode'] = $dados['senderAreaCode'];
		$dadosArray['senderPhone'] = $dados['senderPhone'];
		$dadosArray['senderEmail'] = $dados['senderEmail'];
		$dadosArray['senderHash'] = $dados['hashCartao'];
		$dadosArray['shippingAddressRequired'] = false;
		/*$dadosArray['shippingAddressStreet'] = $dados['shippingAddressStreet'];
		$dadosArray['shippingAddressNumber'] = $dados['shippingAddressNumber'];
		$dadosArray['shippingAddressComplement'] = $dados['shippingAddressComplement'];
		$dadosArray['shippingAddressDistrict'] = $dados['shippingAddressDistrict'];
		$dadosArray['shippingAddressPostalCode'] = $dados['shippingAddressPostalCode'];
		$dadosArray['shippingAddressCity'] = $dados['shippingAddressCity'];
		$dadosArray['shippingAddressState'] = $dados['shippingAddressState'];
		$dadosArray['shippingAddressCountry'] = $dados['shippingAddressCountry'];
		$dadosArray['shippingType'] = $dados['shippingType'];
		$dadosArray['shippingCost'] = $dados['shippingCost'];*/
		$dadosArray['creditCardToken'] = $dados['tokenCartao'];
		$dadosArray['installmentQuantity'] = $dados['qntParcelas'];
		$dadosArray['installmentValue'] = $dados['valorParcelas'];
		$dadosArray['noInterestInstallmentQuantity'] = $dados['noIntInstalQuantity'];
		$dadosArray['creditCardHolderName'] = $dados['creditCardHolderName'];
		$dadosArray['creditCardHolderCPF'] = $dados['creditCardHolderCPF'];
		$dadosArray['creditCardHolderBirthDate'] = $dados['creditCardHolderBirthDate'];
		$dadosArray['creditCardHolderAreaCode'] = $dados['senderAreaCode'];
		$dadosArray['creditCardHolderPhone'] = $dados['senderPhone'];
		$dadosArray['billingAddressStreet'] = $dados['billingAddressStreet'];
		$dadosArray['billingAddressNumber'] = $dados['billingAddressNumber'];
		$dadosArray['billingAddressComplement'] = $dados['billingAddressComplement'];
		$dadosArray['billingAddressDistrict'] = $dados['billingAddressDistrict'];
		$dadosArray['billingAddressPostalCode'] = $dados['billingAddressPostalCode'];
		$dadosArray['billingAddressCity'] = $dados['billingAddressCity'];
		$dadosArray['billingAddressState'] = $dados['billingAddressState'];
		$dadosArray['billingAddressCountry'] = $dados['billingAddressCountry'];

		$buildQuery = http_build_query($dadosArray);
		$url = "https://ws.pagseguro.uol.com.br/v2/transactions";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded; charset=UTF-8"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $buildQuery);
		$retorno = curl_exec($curl);
		curl_close($curl);
		$xml = simplexml_load_string($retorno);

		if(isset($xml->error)){
			
			$retorna = ['erro' => false, 'dados' => $xml];
			header('Content-Type: application/json');
			echo json_encode($retorna);
		}else{
			$dadosInsert = array(	'plano' 	 		=> 	$this->input->post('plano_desc'),
									'valor' 	 		=> 	$dados['itemAmount1'],
									'usuario_id' 		=> 	$this->input->post('usuario_id'),
									'status_id'  		=>	$xml->status,
									'code_trans' 		=> 	$xml->code,
									'tipo_pg'		    => 	$xml->type,
									'forma_pg_cartao' 	=> 	$xml->installmentCount 	);
			
			if( $id_pedido =  $this->pagamentosM->inserir($dadosInsert) ){
				// envia e-mail para o usuário e para os gestores
				// $chama função para enviar os e-mails
				$parmsEmail = $dadosInsert;
				$parmsEmail['nome'] = $xml->sender->name;
				
				$email 	= $this->load->view('home/confirmacao-email', $parmsEmail, true);				
	    		if(	$this->enviaEmail($dados['senderEmail'],'Inspire - Pedido #'.$id_pedido.' Realizado Com Sucesso!', $email) ){
	    			//envia e-mail para os gestores
	    			$conteudo = "<body style='text-align: center;margin: 0 auto; background: #1761ac; color: #ffffff; font-size: 18px; width: 545px;'>
						<table>
							<tr>
								<td><img src='".base_url('bootstrap/images/logoTopo.png')."'></td>
							</tr>
							<tr>
								<td><h4>Compra Realizada no Site</h4></td>
							</tr>
							<tr>
								<td>Pedido: #".$id_pedido."</td>
							</tr>
							<tr>
								<td>Cliente: ".$xml->sender->name."</td>
							</tr>							
							<tr>
								<td>Plano: ".$this->input->post('plano_desc')."</td>
							</tr>
							<tr>
								<td>Valor: R$ ".$dados['itemAmount1']."</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td> 
							</tr>
						</table>
					</body>";
					if( $this->enviaEmail('felipe.aguirre.goncalves@gmail.com','Inspire! Compra Realizada no Site.', $conteudo) ){
						$retorna = ['erro' => true, 'dados' => $xml];
						header('Content-Type: application/json');
						echo json_encode($retorna);
					}
				}
			}else{
				$retorna = ['erro' => false, 'dados' => $xml];
				header('Content-Type: application/json');
				echo json_encode($retorna);
			}

		}
		
    }

    public function verifica_cpf_apenas(){
		
		
		if( !$this->valida_cpf($_POST['cpf']) ){
			echo json_encode(array('status' => 'erro',
									 'mensagem' => 'Cpf inválido.'));	
		}else{
			echo json_encode(array('status' => 'sucesso',
									 'mensagem' => 'Cpf valido.'));
		}		
		
	}	

	public function verifica_email(){

		$result = $this->usuariosM->getEmail($_POST['email']);
		
		if( $result['total'] > 0){
			echo json_encode(array(	'status' 	=>	'erro',
									'mensagem'	=>	'E-mail já cadastrado.'));
		
		} else {
			echo json_encode(array(	'status' 	=>	'sucesso',
									'mensagem'	=>	'E-mail não já cadastrado.'));
		}
		
		
	}

	private function valida_cpf($cpf){

 
	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = str_replace('.', '', str_replace('-','',$cpf));	    
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf[$c] * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf[$c] != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
		
	}	

}
?>